const Vigenere = require('caesar-salad').Vigenere;
const password = 'block';
const express = require('express');
const app = express();
const port = 8000;

app.get('/:name', (req, res) => {
  res.send('' + req.params.name);
});

app.get('/encode/:text', (req,res) => {
  const encodeText = Vigenere.Cipher(password).crypt(req.params.text);
  res.send(encodeText)
});

app.get('/decode/:text', (req, res) => {
  const decodeText = Vigenere.Decipher(password).crypt(req.params.text);
  res.send(decodeText)
});

app.listen(port, () => {
  console.log('We are live on ' + port);
});